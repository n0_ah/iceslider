var sliderLinks = {}
sliderLinks.slider = document.querySelector('.slider');

if (sliderLinks.slider) {
  sliderLinks.slidesCont = sliderLinks.slider.querySelector('.slider_slides_cont');
  sliderLinks.slides = Array.from(sliderLinks.slidesCont.children);
  sliderLinks.slidesOri = Array.from(sliderLinks.slidesCont.children);

  var sliderConf = {}
  sliderConf.infinity = sliderLinks.slider.hasAttribute('slider-infinity');
  sliderConf.buttons = sliderLinks.slider.hasAttribute('slider-buttons');
  sliderConf.bullets = sliderLinks.slider.hasAttribute('slider-bullets');
  sliderConf.transition = sliderLinks.slider.getAttribute('slider-transition');
  sliderConf.auto = sliderLinks.slider.getAttribute('slider-auto');
  sliderConf.autoID = 0;
  sliderConf.dimensions = sliderLinks.slider.getBoundingClientRect();
  sliderConf.width = sliderConf.dimensions.width;
  sliderConf.height = sliderConf.dimensions.height;
  sliderConf.length = sliderLinks.slides.length;
  sliderConf.lengthOri = sliderLinks.slidesOri.length;
  sliderConf.jumpLeft = false;
  sliderConf.jumpRight = false;
  sliderConf.pos = 0;

  if (sliderConf.buttons) {
    var newButtonLeft = document.createElement('button');
    var newButtonRight = document.createElement('button');

    newButtonLeft.classList.add('slider_button');
    newButtonRight.classList.add('slider_button');

    newButtonLeft.classList.add('slider_left');
    newButtonRight.classList.add('slider_right');

    sliderLinks.slider.appendChild(newButtonLeft);
    sliderLinks.slider.appendChild(newButtonRight);

    sliderLinks.sliderButtonLeft = newButtonLeft;
    sliderLinks.sliderButtonRight = newButtonRight;
  }

  if (sliderConf.infinity) {
    var newSlide = sliderLinks.slides[0].cloneNode(true);
    sliderLinks.slidesCont.appendChild(newSlide);
    sliderLinks.slides.push(newSlide);
  }

  if (sliderConf.bullets) {
    function sliderActivateBullet(id) {
      sliderAuto();
      sliderLinks.bullets.forEach((bullet) => {
        bullet.classList.remove('active');
      });

      sliderLinks.bullets[0]
      sliderLinks.bullets[id].classList.add('active');
    }

    var bulletsCont = document.createElement('div');
    bulletsCont.classList.add('slider_bullets_cont');
    sliderLinks.bullets = [];

    for (var i = 0; i < sliderConf.lengthOri; i++) {
      var newBullet = document.createElement('div');
      newBullet.classList.add('slider_bullet');
      bulletsCont.appendChild(newBullet);
      sliderLinks.bullets.push(newBullet);
    }

    sliderLinks.bullets.forEach((bullet, bulletID) => {
      bullet.addEventListener('click', () => {
        sliderActivateBullet(bulletID);

        sliderConf.pos = bulletID;
        sliderRender();
      });
    });

    sliderLinks.slider.appendChild(bulletsCont);
    sliderLinks.bulletsCont = bulletsCont;

    sliderActivateBullet(sliderConf.pos);
  }

  if (sliderConf.auto) {
    sliderAuto();
  }

  sliderConf.length = sliderLinks.slides.length;
  sliderConf.contWidth = sliderConf.length * sliderConf.width;

  function sliderCalc() {
    sliderConf.dimensions = sliderLinks.slider.getBoundingClientRect();
    sliderConf.width = sliderConf.dimensions.width;
    sliderConf.height = sliderConf.dimensions.height;
    sliderConf.contWidth = sliderConf.length * sliderConf.width;
    sliderLinks.slidesCont.style.width = sliderConf.contWidth + 'px';

    sliderLinks.slides.forEach((slide) => {
      slide.style.width = sliderConf.width + 'px';
      slide.style.height = sliderConf.height + 'px';
    });

    sliderSetPos();
  }

  function sliderForceStyles() {
    sliderLinks.slidesCont.getBoundingClientRect();
  }

  function sliderSetTransition(transit = false) {
    if (transit) {
      sliderLinks.slidesCont.style.transition = sliderConf.transition;
    } else {
      sliderLinks.slidesCont.style.transition = null;
    }

    sliderForceStyles();
  }

  function sliderSetPos() {
    sliderLinks.slidesCont.style.transform = 'translateX(-' + sliderConf.width * sliderConf.pos + 'px)';
    (sliderConf.bullets ? sliderActivateBullet(sliderConf.pos % sliderConf.lengthOri) : '');
    sliderForceStyles();
  }


  // init
  sliderLinks.slidesCont.style.width = sliderConf.contWidth + 'px';
  sliderSetTransition(true);

  sliderLinks.slides.forEach((slide) => {
    slide.style.width = sliderConf.width + 'px';
    slide.style.height = sliderConf.height + 'px';
  });


  // render
  function sliderPosCorrector() {
    if (sliderConf.pos < 0) {
      sliderConf.pos += sliderConf.length;
      sliderConf.jumpLeft = true;

    } else if (sliderConf.pos >= sliderConf.length) {
      sliderConf.pos = 0;
      sliderConf.jumpRight = true;
    }
  }

  function sliderAuto() {
    if (sliderConf.auto) {
      clearTimeout(sliderConf.autoID);

      sliderConf.autoID = setTimeout(sliderGoRight, 1000 * Number(sliderConf.auto));
    }
  }

  function sliderRender() {
    if (sliderConf.infinity) {
      if (sliderConf.jumpLeft) {
        sliderSetTransition(false);
        sliderConf.pos = sliderConf.length - 1;
        sliderSetPos();

        sliderSetTransition(true);
        sliderConf.pos--;

      } else if (sliderConf.jumpRight) {
        sliderSetTransition(false);
        sliderConf.pos = 0;
        sliderSetPos();

        sliderSetTransition(true);
        sliderConf.pos++;
      }
    }

    sliderSetPos();

    sliderConf.jumpLeft = false;
    sliderConf.jumpRight = false;
  }

  // event listeners
  function sliderGoLeft() {
    sliderAuto();
    sliderConf.pos--;
    sliderPosCorrector();
    sliderRender();
  }

  function sliderGoRight() {
    sliderAuto();
    sliderConf.pos++;
    sliderPosCorrector();
    sliderRender();
  }

  window.addEventListener('keyup', (e) => {
    if (e.key === 'ArrowLeft') {
      sliderGoLeft();

    } else if (e.key === 'ArrowRight') {
      sliderGoRight();
    }
  });

  if (sliderConf.buttons) {
    sliderLinks.sliderButtonLeft.addEventListener('click', sliderGoLeft);
    sliderLinks.sliderButtonRight.addEventListener('click', sliderGoRight);
  }

  window.addEventListener('resize', sliderCalc);
}
